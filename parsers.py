import urllib
import cfscrape

def getAnimeCapis(anime_name): #anime_name must be with "-" instead of " ".
    scraper = cfscrape.create_scraper()
    page = scraper.get("http://animeflv.net/anime/" + anime_name + ".html").content
    #page = urllib.urlopen("http://animeflv.net/anime/" + anime_name + ".html").read()
    index = page.find('<ul class=\"anime_episodios\"')
    index2 = page.find('</ul>', index)
    
    block = page[index:index2]
    
    index = block.find(anime_name)
    index += len(anime_name) + 1
    index2 = block.find('.html\">', index)
    num = block[index:index2]

    return int(num)
    

def parse_videoHtml(videoHtml, host, verbose):
    list_hosts = [["vkontakte", VK_parse],["hyperion", HY_parse],["rutube", RU_parse],["zippyshare", ZY_parse],["videobam", VBM_parse],["nowvideo", NV_parse],["firedrive", FD_parse],["novamov", NVM_parse],["AFLV", AFLV_parse],["izanagi", IZG_parse],["kami", KM_parse]]
    func = 0
    for i in list_hosts:
        if i[0] == host:
            func = i[1]
            break
        
    return func(videoHtml, verbose)

#####################
# Video web parsers #
#####################

def KM_parse(videoHtml, verbose):
    if verbose:
        print "Parsing kami video...\n"
    indexIZG = videoHtml.find('"kami"')
    if indexIZG == -1:
        print "No kami link in this episode..."
        exit(1)
    else:
        indexIZG1 = videoHtml.find("https:", indexIZG)
        indexIZG2 = videoHtml.find('" height', indexIZG1)

        videoIZGHtml = videoHtml[indexIZG1:indexIZG2]

        videoIZGHtml = videoIZGHtml.replace("\\/", "/")
        
        scraper = cfscrape.create_scraper()
        page2 = scraper.get(videoIZGHtml).content
        
        index1 = page2.find("jwplayer('player').setup(")
        index2 = page2.find("http://", index1)
        index3 = page2.find('",', index2)
        print page2
        return page2[index2:index3]


def IZG_parse(videoHtml, verbose):
    if verbose:
        print "Parsing izanagi video...\n"
    indexIZG = videoHtml.find('"izanagi"')
    if indexIZG == -1:
        print "No izanagi link in this episode..."
        exit(1)

    else:
        indexIZG1 = videoHtml.find("https:", indexIZG)
        indexIZG2 = videoHtml.find('" height', indexIZG1)

        videoIZGHtml = videoHtml[indexIZG1:indexIZG2]
        print videoIZGHtml
        videoIZGHtml = videoIZGHtml.replace("\\/", "/")
        videoIZGHtml = videoIZGHtml.replace(",","")
        videoIZGHtml = videoIZGHtml.replace("\\","")
        print videoIZGHtml
        scraper = cfscrape.create_scraper()
        page2 = scraper.get(videoIZGHtml).content
        
        index1 = page2.find("jwplayer('player').setup(")
        index2 = page2.find("http://", index1)
        index3 = page2.find("',", index2)
        print page2[index2:index3]
        return page2[index2:index3]

        
def AFLV_parse(videoHtml, verbose):
    if verbose:
        print "Parsing AFLV video...\n"
    indexAFLV = videoHtml.find("AFLV")
    if indexAFLV == -1:
        print "No AFLV link in this episode..."
        exit(1)
    else:
        indexAFLV1 = videoHtml.find("http:", indexAFLV)
        indexAFLV2 = videoHtml.find('\" height', indexAFLV1)

        videoAFLVHtml = videoHtml[indexAFLV1:indexAFLV2]

        videoAFLVHtml = videoAFLVHtml.replace("\\", "")
        
        scraper = cfscrape.create_scraper()
        page2 = scraper.get(videoAFLVHtml).content
        
        index1 = page2.find("jwplayer('player').setup(")
        index2 = page2.find("http://", index1)
        index3 = page2.find("',", index2)

        return page2[index2:index3]
        

def VK_parse(videoHtml, verbose):
    if verbose:
        print "Parsing VK video...\n"
    indexVK = videoHtml.find("vkontakte")
    if indexVK == -1:
        print "No VK link in this episode..."
        exit(1)
        
    else:
        indexVKurl1 = videoHtml.find("http:", indexVK)
        indexVKurl2 = videoHtml.find("\\\"", indexVKurl1)
        
        videoHtmlVK = videoHtml[indexVKurl1:indexVKurl2]
        
        videoHtmlVK = videoHtmlVK.replace("\\", "")
            
        
        page2 = urllib.urlopen(videoHtmlVK).read()
        
        errorURL = 0            
            
        index = page2.find("url720=")
        if index == -1:
            index = page2.find("url480=")
            if index == -1:
                index = page2.find("url360=")
                if index == -1:
                    index = page2.find("url240=")
                    if index == -1:
                        errorURL = 1

        if errorURL == 0:
            index += len("url240=")
            index2 = page2.find("?extra", index)
                
            return page2[index:index2]
            
        else:
            print "Error parsing VK video:", videoHtmlVK, "\nMaybe video deleted?"
            exit(1)
       
def HY_parse(videoHtml, verbose):
    if verbose:
        print "Parsing hyperion video...\n"
    indexHY = videoHtml.find("hyperion")
    if indexHY == -1:
        print "No hyperion link in this episode..."
        exit(1)
        
    else:
        indexUrl1 = videoHtml.find("file=\\", indexHY)
        indexUrl1 += len("file=\\")
        indexUrl2 = videoHtml.find("&provider", indexUrl1)
        
        linkHY = videoHtml[indexUrl1:indexUrl2]
        linkHY = linkHY.replace("\\", "")
        linkHY = linkHY.replace("%252F", "%2F")
        linkHY = linkHY.replace("%253D", "")
        linkHY = linkHY.replace("%252B", "%2B")
        
        linkTotal = "http://animeflv.net" + linkHY
        
        return linkTotal
    
def RU_parse(videoHtml, verbose):
    print "RU not implemented yet."
    return 0

def ZY_parse(videoHtml, verbose):
    print "ZY not implemented yet."
    return 0
    
def VBM_parse(videoHtml, verbose):
    if verbose:
        print "Parsing videobam video..."
    indexVBM = videoHtml.find("videobam")
    if indexVBM == -1:
        print "No videobam link in this episode..."
        exit(1)
        
    else:
        index11 = videoHtml.find("src=\"", indexVBM)
        index11 += len("src=\"")
        index21 = videoHtml.find("\" ", index11)
        
        linkVBM = videoHtml[index11:index21]
        linkVBM = linkVBM.replace("\/", "/")
        
        page2 = urllib.urlopen(linkVBM).read()
        
        index12 = page2.find("var player_config")
        index12 = page2.find("url", index12)
        index12 += 1
        index12 = page2.find("url:", index12)
        index12 = page2.find("http:", index12)
        index22 = page2.find("autoBuffering", index12)
        index22 -= len("\",\"")
        cut = page2[index12:index22]
        
        cut = cut.replace("\/", "/")
        
        if not cut:
            print "Error parsing videobam video.\nMaybe video deleted?"
            exit(1)
        
        return cut

def NV_parse(videoHtml, verbose):
    # It's nearly done, but we need a way to download a dynamic website's source code.
    # I dont want to use extern and weird libraries, so I'm on it.
    print "NV not implemented yet."
    exit(1)
    
    indexNV = videoHtml.find("nowvideo")
    if indexNV == -1:
        print "No nowvideo link in this episode..."
        exit(1)
        
    else:
        index11 = videoHtml.find("src=\"", indexNV)
        index11 += len("src=\"")
        index21 = videoHtml.find("\" ", index11)
        
        linkNV = videoHtml[index11:index21]
        
        index12 = linkNV.find("?v=")
        index12 += len("?v=")
        index22 = linkNV.find("&width", index12)
        videoID = linkNV[index12:index22]
        
        mobileUrl = "http://www.nowvideo.sx/mobile/#/videos/" + videoID      
        
        page2 = urllib.urlopen(mobileUrl).read()
                
        index13 = page2.find("video.php?")
        index23 = page2.find("\" ", index13)
        
        print page2
        
    return 0

def FD_parse(videoHtml, verbose):
    print "FD not implemented yet."
    return 0
        
def NVM_parse(videoHtml, verbose):
    print "NVM not implemented yet."
    return 0

                
            
