#########################
# ANIME BOOKMARK SAVING #
#########################

import os

def check_file(f):
    if os.path.exists(f):
        return 1
    return 0

def save(name, ep, archive):
    
    if check_file(archive) == 0:
        f = open(archive, mode='w')
        print "Bookmarks file does not exist. Creating it.."
        f.close()
    
    line = 0
    f = open(archive, mode='r+')
    for i in f:
        if i.find(name) != -1:
            line = 1
            break
        
        
    if line == 1:
        f.seek(0)
        oldfile = f.read()
        i = oldfile.find(name)
        i += len(name) + 1 # +1 cause the \t
        oldfile = list(oldfile)
        oldfile[i] = str(ep)
        oldfile = ''.join(oldfile)
        f.close()
        f = open(archive, mode='w')
        f.write(oldfile)
        
    else:
        f.write(name + "\t" + str(ep) + "\n")
        
    print "Bookmark saved successfully."
    f.close()
    

def load(name, archive):
    if check_file(archive) == 0:
        f = open(archive, mode='w')
        f.close()
    
    line = -1
    f = open(archive, mode='r+')
    for i in f:
        if i.find(name) != -1:
            line = i
            break
        
    if line != -1:
        end = line.find("\n")
        intlen = end - len(name) - 1
        n = 0
        for i in range(0, intlen):
            n += int(line[len(name) + 1 + i]) * (10**(intlen - 1 - i))
        return n
    else:
        print "Bookmark not found. Initialising the anime to episode 1.."
        save(name, 1, archive)
        return 1