#!/usr/bin/python

import mimetypes
import urllib
import os
import argparse
import re
import parsers as p
import bookmarks as b
import threading
import cfscrape

defaulthost = "izanagi"

mimetypes.init()

def getHosts(Html):
    listHost = []
    spleht = re.split('\d\d\\\/\d\d\\\/\d\d\d\d\",\"', Html)
    for i in range(1, len(spleht) - 1):
        indexFin = spleht[i].find("\"")
        listHost.append(spleht[i][0:indexFin])
    return listHost
    


def parseOnePage(anime_name, n_ep, verbose):
    scraper = cfscrape.create_scraper()
    page = scraper.get("http://animeflv.net/ver/" + anime_name + "-" + n_ep + ".html").content
    #page = urllib.urlopen("http://animeflv.net/ver/" + anime_name + "-" + n_ep + ".html").read()
    index = page.find("var videos =")
    if index == -1:
        print "Error parsing the webpage, is it AnimeFLV?"
        print "http://animeflv.net/ver/" + anime_name + "-" + n_ep + ".html"
        print page
        exit(1)
            
    else:
        index2 = page.find("};", index)
        
        if index2 == -1:
            print "Error parsing the webpage, is it AnimeFLV?"
            exit(1)
            
        else:
            index2 += len("};")
            videosHtml = page[index:index2]
    ###
    # From here is video parsing.
    ###
            hosts = getHosts(videosHtml)
            indexVK = 0
            cont = 0
            string_input = "Select a host:\n"
            for i in range(0, len(hosts)):
                string_input += (str(i)  + ". " + hosts[i])
                if hosts[i] == defaulthost:
                    string_input += " (default)"
                    indexVK = i
                string_input += "\n"
                cont += 1
            if verbose:
                opt = raw_input(string_input + "--> ")
                #if opt == '' or opt > cont or opt < 0 or opt != int(opt):
                #    opt = indexVK
                opt = int(opt)
                print "Selected", hosts[opt], "\n"
            if not verbose:
                opt = indexVK
            urlVideo = p.parse_videoHtml(videosHtml, hosts[opt], verbose)
            
    ###
    # End of parsing.
    ###
            if urlVideo != 0:
                if verbose:
                    print "Video Link:\n", urlVideo
                if args.download:
                    extension = urlVideo.split(".")[-1]
                    formats = ["avi", "mp4", "flv", "webm", "mkv", "vob", "ogg", "mov", "wmv", "mpg", "mpeg", "mpv", "3gp"]
                    if extension not in formats:
                        if verbose:
                            print "Video extension unknown!", extension, "\nIt will be saved with the 'mp4' extension."
                        extension = "mp4"
                    
                    if verbose:
                        os.system('wget -c "' + urlVideo + '" -O ' + args.download[0] + "/" + anime_name + "_" + n_ep + "."  + extension)
                    else:
                        print "-- Downloading episode " + n_ep + " of " + anime_name
                        #urllib.urlretrieve(urlVideo, args.download[0] + "/" + anime_name + "_" + n_ep + "." + extension)
                        os.system("wget -c " + urlVideo + " -O " + args.download[0] + "/" + anime_name + "_" + n_ep + "."  + extension)
                        print "V-- Episode " + n_ep + " download completed."
                else:
                    #os.system("nohup " + args.player + " " + urlVideo + " >/dev/null 2>&1 &")
                    os.system(args.player + " " + urlVideo)
                
    
def parseAllPage(anime_name):
    ncapis = p.getAnimeCapis(anime_name)
    print "Downloading " + str(ncapis) + " episodes of " + anime_name + "\n"
    for j in range(1, ncapis+1):
        parseOnePage(anime_name, str(j), 0)
                
        
####
# At the moment, this program only works with VK videos, using VLCs the media player.
# In the future I'll add support to the other pages, as well as your own media player.
####

###
# Args parser
###
parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument('-w', '--watch', action="store", nargs=2, metavar=('anime_name', 'ep_num'), help='Watch a specified anime from AnimeFLV.')
group.add_argument('-d', '--download', action="store", nargs=3, metavar=('path', 'anime_name', 'ep_num'), help='Download the video to \"path\" (must be a valid path) instead of playing it. If you want to download every episode of the anime, put \"a\" in n_ep')
parser.add_argument('-p', '--player', action="store", nargs='?', default='mpv --vo=vaapi --hwdec=vaapi --hwdec-codecs=all', help='Play with PLAYER instead of MPV.')
group.add_argument('-bs', '--bookmarkSave', action="store", nargs=2, metavar=('anime_name', 'ep_num'), help='Edit a bookmark or set it if it\'s not set already.')
group.add_argument('-bl', '--bookmarkLoad', action="store", nargs=1, metavar='anime_name', help='Look for a bookmark. If it\'s not set, set it to 1 (first ep.) and return it.')

args = parser.parse_args()

###
# Correct args and options setted wisely.
# Let's find the goddamn vid!
###


# Bookmarks mode.
if args.bookmarkSave:
    nombre_anime = args.bookmarkSave[0]
    nombre_anime = nombre_anime.lower()
    nombre_anime = nombre_anime.replace(" ", "-")
    nombre_anime = nombre_anime.replace(".", "-")
    nombre_anime = nombre_anime.replace(":", "")
    n_ep = args.bookmarkSave[1]
    b.save(nombre_anime, int(n_ep), "book.marks")
elif args.bookmarkLoad:
    nombre_anime = args.bookmarkLoad[0]
    nombre_anime = nombre_anime.lower()
    nombre_anime = nombre_anime.replace(" ", "-")
    nombre_anime = nombre_anime.replace(".", "-")
    nombre_anime = nombre_anime.replace(":", "")
    print b.load(nombre_anime, "book.marks")
    
# No bookmark opt.
elif args.watch:
    nombre_anime = args.watch[0]
    n_ep = args.watch[1]
    nombre_anime = nombre_anime.lower()
    nombre_anime = nombre_anime.replace(" ", "-")
    nombre_anime = nombre_anime.replace(".", "-")
    nombre_anime = nombre_anime.replace(":", "")
    
    parseOnePage(nombre_anime, n_ep, 1)
    
# Downloading.
elif args.download:
    path = args.download[0]
    nombre_anime = args.download[1]
    n_ep = args.download[2]
    nombre_anime = nombre_anime.lower()
    nombre_anime = nombre_anime.replace(" ", "-")
    nombre_anime = nombre_anime.replace(".", "-")
    nombre_anime = nombre_anime.replace(":", "")

    args.download[0] = args.download[0] + "/" + nombre_anime
    os.system("mkdir " + args.download[0])
    
    if n_ep=='a':
        parseAllPage(nombre_anime)
    else:
        parseOnePage(nombre_anime, n_ep, 1)
        

